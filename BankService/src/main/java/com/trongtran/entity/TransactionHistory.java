package com.trongtran.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction_history")
public class TransactionHistory {
    @Id
    private String txid;
    private String tx_name;
    private String status;
    private Double total_money;
    private Double fee;
    private Timestamp time;
    @JoinColumn(name = "account_number",referencedColumnName = "account_number")
    @ManyToOne(fetch = FetchType.EAGER)
    private BankAccount bankAccount;

    public TransactionHistory() {
    }

    public String getTxid() {
        return txid;
    }

    public void setTxid(String txid) {
        this.txid = txid;
    }

    public String getTx_name() {
        return tx_name;
    }

    public void setTx_name(String tx_name) {
        this.tx_name = tx_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getTotal_money() {
        return total_money;
    }

    public void setTotal_money(Double total_money) {
        this.total_money = total_money;
    }

    public Double getFee() {
        return fee;
    }

    public void setFee(Double fee) {
        this.fee = fee;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
    @JsonIgnore
    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
}
