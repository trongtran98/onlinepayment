package com.trongtran.entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "bank_account")
public class BankAccount {
    @Id
    private String account_number;
    private String pin_code;
    private String user_name;
    private String address;
    private Double amount;
    @OneToMany(mappedBy = "bankAccount", fetch = FetchType.EAGER)
    private Set<TransactionHistory> transactionHistories;

    public BankAccount() {}

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getPin_code() {
        return pin_code;
    }

    public void setPin_code(String pin_code) {
        this.pin_code = pin_code;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }


    public Set<TransactionHistory> getTransactionHistories() {
        return transactionHistories;
    }

    public void setTransactionHistories(Set<TransactionHistory> transactionHistories) {
        this.transactionHistories = transactionHistories;
    }
}
