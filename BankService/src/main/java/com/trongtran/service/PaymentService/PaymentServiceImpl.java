package com.trongtran.service.PaymentService;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.trongtran.entity.BankAccount;
import com.trongtran.entity.TransactionHistory;
import com.trongtran.repository.BankAccountRepo.BankAccountRepo;
import com.trongtran.repository.TransactionHistoryRepo.TransactionHistoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class PaymentServiceImpl implements PaymentService{
    @Autowired
    BankAccountRepo bankAccountRepo;
    @Autowired
    TransactionHistoryRepo transactionHistoryRepo;

    @Transactional(rollbackFor = Exception.class)
    public String trade(BankAccount customer, BankAccount partner, String order_id, double total_money) {

        BankAccount customer_verify = bankAccountRepo.verifyAccount(customer.getAccount_number(),customer.getPin_code());
        BankAccount partner_verify = bankAccountRepo.verifyAccount(partner.getAccount_number(),partner.getPin_code());

        if(customer_verify!=null && partner_verify!=null){
            double cus_amount = customer_verify.getAmount();
            double fee;
            if(total_money<=20)
                fee = 1;
            else if (total_money>20 && total_money <=100)
                fee = total_money/100*2;
            else if(total_money>100 && total_money<=300)
                fee = total_money/100*1.5;
            else if(total_money>500 && total_money<=1000)
                fee = total_money/100;
            else
                fee = total_money/100*0.5;

            if(cus_amount-(total_money+fee)>=0){
                //subtract money
                customer_verify.setAmount(cus_amount-(total_money+fee));
                //transaction of customer
                TransactionHistory transaction_cus = new TransactionHistory();
                transaction_cus.setBankAccount(customer_verify);
                transaction_cus.setTxid("tx"+System.currentTimeMillis());
                transaction_cus.setFee(fee);
                transaction_cus.setStatus("transfer");
                transaction_cus.setTx_name("tx_"+order_id);
                transaction_cus.setTotal_money(total_money);

                transaction_cus.setTime(new Timestamp(new Date().getTime()));
                transactionHistoryRepo.save(transaction_cus);

                //addition money
                partner_verify.setAmount(partner_verify.getAmount()+total_money);
                //transaction of partner
                TransactionHistory transaction_par = new TransactionHistory();
                transaction_par.setTxid("tx"+System.currentTimeMillis());
                transaction_par.setBankAccount(partner_verify);
                transaction_par.setFee((double) 0);
                transaction_par.setStatus("receive");
                transaction_par.setTx_name("tx_"+order_id);
                transaction_par.setTotal_money(total_money);

                transaction_cus.setTime(new Timestamp(new Date().getTime()));
                transactionHistoryRepo.save(transaction_par);

                ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                String json =null;
                try {
                    json = ow.writeValueAsString(transaction_cus);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
                return json;
            }else{
                throw new ArithmeticException();
            }
        }

        return "wrong account";
    }
}
