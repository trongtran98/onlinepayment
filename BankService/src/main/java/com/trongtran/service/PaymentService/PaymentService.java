package com.trongtran.service.PaymentService;

import com.trongtran.entity.BankAccount;
import org.springframework.stereotype.Service;

@Service
public interface PaymentService {
    String trade(BankAccount customer,BankAccount partner,String order_id, double total_money);
}
