package com.trongtran.service.TransactionHistoryService;

import com.trongtran.entity.BankAccount;
import com.trongtran.entity.TransactionHistory;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Set;

@Service
public interface TransactionHistoryService {
    Set<TransactionHistory> getTransactionByTime(BankAccount bankAccount, Timestamp begin, Timestamp end);
}
