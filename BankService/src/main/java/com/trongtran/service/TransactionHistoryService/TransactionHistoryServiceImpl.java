package com.trongtran.service.TransactionHistoryService;

import com.trongtran.entity.BankAccount;
import com.trongtran.entity.TransactionHistory;
import com.trongtran.repository.TransactionHistoryRepo.TransactionHistoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Set;

@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {
    @Autowired
    TransactionHistoryRepo transactionHistoryRepo;
    public Set<TransactionHistory> getTransactionByTime(BankAccount bankAccount, Timestamp begin, Timestamp end) {
        return transactionHistoryRepo.getTransactionByTime(bankAccount,begin,end);
    }
}
