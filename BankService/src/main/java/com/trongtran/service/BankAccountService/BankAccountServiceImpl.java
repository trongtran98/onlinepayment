package com.trongtran.service.BankAccountService;

import com.trongtran.entity.BankAccount;
import com.trongtran.repository.BankAccountRepo.BankAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BankAccountServiceImpl implements BankAccountService {
    @Autowired
    BankAccountRepo bankAccountRepo;

    public BankAccount verifyAccount(String account_number, String pin_code) {
        return bankAccountRepo.verifyAccount(account_number,pin_code);
    }

    public Double getBalance(String account_number, String pin_code) {
        return bankAccountRepo.getBalance(account_number,pin_code);
    }
}
