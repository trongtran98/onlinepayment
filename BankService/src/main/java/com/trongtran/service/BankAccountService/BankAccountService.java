package com.trongtran.service.BankAccountService;

import com.trongtran.entity.BankAccount;
import org.springframework.stereotype.Service;

@Service
public interface BankAccountService {
    BankAccount verifyAccount(String account_number, String pin_code);
    Double getBalance(String account_number, String pin_code);
}
