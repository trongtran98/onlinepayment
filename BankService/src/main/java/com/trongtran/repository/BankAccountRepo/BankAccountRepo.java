package com.trongtran.repository.BankAccountRepo;

import com.trongtran.entity.BankAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BankAccountRepo extends CrudRepository<BankAccount,String> {
    @Query(value = "from BankAccount b where b.account_number = :account_number and b.pin_code = :pin_code")
    BankAccount verifyAccount(@Param("account_number") String account_number,@Param("pin_code") String pin_code);

    @Query(value = "select b.amount from BankAccount b where b.account_number = :account_number and b.pin_code = :pin_code")
    Double getBalance(@Param("account_number") String account_number,@Param("pin_code") String pin_code);
}
