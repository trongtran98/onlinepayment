package com.trongtran.repository.TransactionHistoryRepo;

import com.trongtran.entity.BankAccount;
import com.trongtran.entity.TransactionHistory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Set;

@Repository
public interface TransactionHistoryRepo extends CrudRepository<TransactionHistory,String> {

    @Query("from TransactionHistory t where t.bankAccount = :account_number and time between :begin and :end order by t.time desc")
    Set<TransactionHistory> getTransactionByTime(@Param("account_number") BankAccount account_number, @Param("begin") Timestamp begin, @Param("end") Timestamp end);

}
