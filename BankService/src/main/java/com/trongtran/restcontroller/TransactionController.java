package com.trongtran.restcontroller;

import com.trongtran.entity.BankAccount;
import com.trongtran.entity.TransactionHistory;
import com.trongtran.service.BankAccountService.BankAccountService;
import com.trongtran.service.TransactionHistoryService.TransactionHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@RestController
public class TransactionController {
    @Autowired
    TransactionHistoryService transactionHistoryService;
    @Autowired
    BankAccountService bankAccountService;

    @RequestMapping(value = "/transaction/{account}/{pin}/{begin}/{end}",method = RequestMethod.GET)
    public Set<TransactionHistory> getTransactionByTime(@PathVariable("account") String account,
                                                        @PathVariable("pin") String pin,
                                                        @PathVariable("begin") String begin,
                                                        @PathVariable("end") String end) {
        BankAccount account_verify = bankAccountService.verifyAccount(account, pin);
        if(account_verify!=null){
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 =  dateFormat.parse(begin);
                Date date2 =  dateFormat.parse(end);
                Timestamp begin_time = new Timestamp(date1.getTime());
                Timestamp end_time = new Timestamp(date2.getTime());
                return transactionHistoryService.getTransactionByTime(account_verify, begin_time,end_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return new HashSet<TransactionHistory>();
    }
}
