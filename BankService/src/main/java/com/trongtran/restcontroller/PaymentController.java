package com.trongtran.restcontroller;

import com.trongtran.entity.BankAccount;
import com.trongtran.service.PaymentService.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @RequestMapping(value = "/trade/{customer_acc}/{customer_pin}/{partner_acc}/{partner_pin}/{order_id}/{total_money}")
    public String Hello(@PathVariable String customer_acc,@PathVariable String customer_pin,
                        @PathVariable String partner_acc,@PathVariable String partner_pin,
                        @PathVariable String order_id,@PathVariable Double total_money){
        BankAccount customer = new BankAccount();
        customer.setAccount_number(customer_acc);
        customer.setPin_code(customer_pin);

        BankAccount partner = new BankAccount();
        partner.setAccount_number(partner_acc);
        partner.setPin_code(partner_pin);
        try {
            return paymentService.trade(customer,partner,order_id,total_money);
        }catch (Exception e){
            return "account is not enough money";
        }
    }
}
