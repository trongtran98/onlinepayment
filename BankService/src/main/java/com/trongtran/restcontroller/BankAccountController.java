package com.trongtran.restcontroller;

import com.trongtran.service.BankAccountService.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankAccountController {
    @Autowired
    BankAccountService bankAccountService;

    @RequestMapping(value = "/balance/{customer_acc}/{customer_pin}")
    public String getBalance(@PathVariable String customer_acc, @PathVariable String customer_pin){
        return String.valueOf(bankAccountService.getBalance(customer_acc,customer_pin));
    }
}
