<%--
  Created by IntelliJ IDEA.
  User: TrongTran
  Date: 7/14/2018
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>

    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
</head>
<body>

<div style="margin-top: 50px;" class="container">

    <!-- You can make it whatever width you want. I'm making it full width
         on <= small devices and 4/12 page width on >= medium devices -->
    <div class="col-xs-12 col-md-4">
        <!-- CREDIT CARD FORM STARTS HERE -->
        <div class="panel panel-default credit-card-box">
            <div class="panel-heading display-table">
                <div class="row display-tr">
                    <h3 class="panel-title display-td">Payment Form</h3>
                    <div class="display-td">
                        <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form role="form" id="payment-form" method="POST" action="/trade">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="cardNumber">CARD NUMBER</label>
                                <div class="input-group">
                                    <input
                                            id="cardNumber"
                                            type="tel"
                                            class="form-control"
                                            name="customer_acc"
                                            placeholder="Valid Card Number"
                                            autocomplete="cc-number"
                                            required autofocus
                                    />
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 col-md-7 pull-right">
                            <div class="form-group">
                                <label for="cardExpiry"><span class="hidden-xs">Total Money</span><span
                                        class="visible-xs-inline">EXP</span> </label>
                                <div class="input-group">
                                    <input
                                            id="cardExpiry"
                                            type="tel"
                                            class="form-control"
                                            name="total_money"
                                            placeholder="Enter the money"
                                            autocomplete="cc-exp"
                                            required
                                    />
                                    <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-7 col-md-5">
                            <div class="form-group">
                                <label for="cardCVC">Order Code</label>
                                <input
                                        id="cardCVC"
                                        type="tel"
                                        class="form-control"
                                        name="order_id"
                                        placeholder="Ex: Order001"
                                        autocomplete="cc-csc"
                                        required
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="couponCode">PIN CODE</label>
                                <input id="couponCode" required type="password" class="form-control"
                                       name="customer_pin"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <input class="subscribe btn btn-success btn-lg btn-block" type="submit"
                                   value="Start Trading"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="payment-errors">${error}</p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- CREDIT CARD FORM ENDS HERE -->

    </div>


    <form class="form-horizontal" action="/transaction" method="post" role="form">
        <fieldset>
            <legend style="margin-left: 50px;">Lookup History</legend>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="card-holder-name">CARD NUMBER</label>
                <div class="col-sm-9">
                    <input required type="text" class="form-control" name="account" id="card-holder-name"
                           placeholder="Debit/Credit Card Number">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="card-number">PIN CODE</label>
                <div class="col-sm-9">
                    <input required type="password" class="form-control" name="pin" id="card-number" placeholder="PIN CODE">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="expiry-month">TIME</label>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-xs-4">
                            <input required type="date" class="form-control col-sm-2" name="begin" id="expiry-month"/>
                        </div>
                        <div class="col-xs-4">
                            <input required type="date" class="form-control col-sm-2" name="end"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <input type="submit" class="btn btn-success" value="Lookup Now"/>
                </div>
            </div>
        </fieldset>
    </form>


</div>


</body>

<style>
    /* Padding - just for asthetics on Bootsnipp.com */
    body {
        margin-top: 20px;
    }

    /* CSS for Credit Card Payment form */
    .credit-card-box .panel-title {
        display: inline;
        font-weight: bold;
    }

    .credit-card-box .form-control.error {
        border-color: red;
        outline: 0;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 8px rgba(255, 0, 0, 0.6);
    }

    .credit-card-box label.error {
        font-weight: bold;
        color: red;
        padding: 2px 8px;
        margin-top: 2px;
    }

    .credit-card-box .payment-errors {
        font-weight: bold;
        color: red;
        padding: 2px 8px;
        margin-top: 2px;
    }

    .credit-card-box label {
        display: block;
    }

    /* The old "center div vertically" hack */
    .credit-card-box .display-table {
        display: table;
    }

    .credit-card-box .display-tr {
        display: table-row;
    }

    .credit-card-box .display-td {
        display: table-cell;
        vertical-align: middle;
        width: 50%;
    }

    /* Just looks nicer */
    .credit-card-box .panel-heading img {
        min-width: 180px;
    }
</style>

</html>
