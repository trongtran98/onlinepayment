<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%--
  Created by IntelliJ IDEA.
  User: TrongTran
  Date: 7/14/2018
  Time: 3:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4>Transaction History: </h4>
            <strong>From ${begin} to ${end}</strong>
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                    <th>Transaction ID</th>
                    <th>Transaction Name</th>
                    <th>Status</th>
                    <th>Total Money</th>
                    <th>Fee</th>
                    <th>Time</th>
                    </thead>
                    <tbody>
                    <c:forEach items="${transactions}" var="tx">
                        <tr>
                            <td>${tx.txid}</td>
                            <td>${tx.tx_name}</td>
                            <td>${tx.status}</td>
                            <fmt:setLocale value="en_US"/>
                            <td><fmt:formatNumber value="${tx.total_money}" type="currency" currencySymbol="$"/></td>
                            <td><fmt:formatNumber value="${tx.fee}" type="currency" currencySymbol="$"/></td>
                            <fmt:parseDate value="${tx.time}" var="dateObject"
                                           pattern="yyyy-MM-dd HH:mm:ss" />
                            <td><fmt:formatDate value="${dateObject }" pattern="dd/MM/yyyy"/> <fmt:formatDate value="${dateObject }" pattern="hh:mm a" /></td>

                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <div class="clearfix"></div>
                <ul class="pagination pull-right">
                <%--<li class="disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span></a></li>--%>
                <%--<li class="active"><a href="#">1</a></li>--%>
                <%--<li><a href="#">2</a></li>--%>
                <%--<li><a href="#">3</a></li>--%>
                <%--<li><a href="#">4</a></li>--%>
                <%--<li><a href="#">5</a></li>--%>
                    <%--<c:forEach begin="1" end="${total_page}" varStatus="i">--%>
                        <%--<li><a href="/transaction/page/${i.index}">${i.index}</a></li>--%>
                    <%--</c:forEach>--%>
                <%--<li><a href="#"><span class="glyphicon glyphicon-chevron-right"></span></a></li>--%>
                </ul>

                <div class="form-group">
                    <div class="col-sm-offset-10">
                        <a href="/">
                            <button type="button" class="btn btn-success">
                                Home page
                            </button>
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
</body>
</html>
