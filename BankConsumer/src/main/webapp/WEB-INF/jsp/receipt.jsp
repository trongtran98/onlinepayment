<%--
  Created by IntelliJ IDEA.
  User: TrongTran
  Date: 7/14/2018
  Time: 9:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <address>
                        <strong>FPT Aptech</strong>
                        <br>
                        08 Ton That Thuyet
                        <br>
                        My Dinh, Tu Liem HN
                        <br>
                        <abbr title="Phone">P:</abbr> (024) 7300 8855
                    </address>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                    <p>
                        <fmt:parseDate value="${receipt.time}" var="dateObject"
                                       pattern="yyyy-MM-dd HH:mm:ss" />
                        <em>Date: <fmt:formatDate value="${dateObject }" pattern="dd/MM/yyyy"/> <fmt:formatDate value="${dateObject }" pattern="hh:mm a" /></em>
                    </p>
                    <p>
                        <em>Receipt #: ${receipt.txid}</em>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                </span>
                <table style="line-height: 2em;" class="table table-hover">
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>#</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="col-md-5"><em>${order_id}</em></h4></td>
                        <td class="col-md-1" style="text-align: center"> 1</td>
                        <td class="col-md-1 text-center">${receipt.total_money} $</td>
                        <td class="col-md-1 text-center">${receipt.total_money} $</td>
                    </tr>

                    <tr>
                        <td>  </td>
                        <td>  </td>
                        <td class="text-right">
                            <p>
                                <strong>Subtotal: </strong>
                            </p>
                            <p>
                                <strong>Fee: </strong>
                            </p></td>
                        <td class="text-center">
                            <p>
                                <strong>${receipt.total_money} $</strong>
                            </p>
                            <p>
                                <strong>${receipt.fee} $</strong>
                            </p></td>
                    </tr>
                    <tr>
                        <td><strong>Balance: </strong>${balance} $</td>
                        <td> </td>
                        <td class="text-right"><h4><strong>Total: </strong></h4></td>
                        <td class="text-center text-danger"><h4><strong>${receipt.total_money+receipt.fee} $</strong></h4></td>
                    </tr>
                    </tbody>
                </table>
                <a href="/">
                <button type="button" class="btn btn-success btn-lg btn-block">
                    Confirm Now   <span class="glyphicon glyphicon-chevron-right"></span>
                </button></a>
                </td>
            </div>
        </div>
    </div>
</div>
</body>
<style>
    body {
        margin-top: 20px;
    }
</style>
</html>
