package com.trongtran.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.trongtran.entity.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;

@Controller
public class TransactionController {
    final String BASE_URI = "http://localhost:8080/transaction/{account}/{pin}/{begin}/{end}";

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    static List<Transaction> result = null;

    @RequestMapping(value = "/transaction", method = RequestMethod.POST)
    public String getTransactionByTime(@RequestParam("account") String account,
                                       @RequestParam("pin") String pin,
                                       @RequestParam("begin") String begin,
                                       @RequestParam("end") String end,
                                       ModelMap modelMap) {

        RestTemplate restTemplate = new RestTemplate();
        String string = restTemplate.getForObject(BASE_URI, String.class, account, pin, begin, end);

        ObjectMapper mapper = new ObjectMapper();
        try {
            result = mapper.readValue(string,new TypeReference<List<Transaction>>(){});
            result.sort(new Comparator<Transaction>() {
                public int compare(Transaction o1, Transaction o2) {
                    if(o1.getTime().equals(o2.getTime())){
                        return 0;
                    }else if(o1.getTime().after(o2.getTime())){
                        return -1;
                    }else {
                        return 1;
                    }
                }
            });
            modelMap.addAttribute("begin",begin);
            modelMap.addAttribute("end",end);
            modelMap.addAttribute("transactions",result);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "lookup_transaction";
    }
}
