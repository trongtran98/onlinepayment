package com.trongtran.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trongtran.entity.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

@Controller
public class PaymentController {
    final String BASE_URI_TRADE = "http://localhost:8080/trade/{customer_acc}/{customer_pin}/{partner_acc}/{partner_pin}/{order_id}/{total_money}";
    final String BASE_URI_BALANCE = "http://localhost:8080/balance/{customer_acc}/{customer_pin}";
    @RequestMapping(value = "/trade",method = RequestMethod.POST)
    public String trade(@RequestParam String customer_acc, @RequestParam String customer_pin,
                        @RequestParam String order_id, @RequestParam Double total_money, ModelMap modelMap) {
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(BASE_URI_TRADE, String.class, customer_acc, customer_pin, "AC987654321", "123456", order_id, total_money);
        if(result.equals("account is not enough money")){
            modelMap.addAttribute("error","Oops! Your account is not enough money.");
            return "index";
        }else if(result.equals("wrong account")){
            modelMap.addAttribute("error","Wrong account! please check your account number and pin code.");
            return "index";
        }else {
            ObjectMapper mapper = new ObjectMapper();
            Transaction transaction=null;
            try {
                transaction = mapper.readValue(result,Transaction.class);
                modelMap.addAttribute("order_id",order_id);
                modelMap.addAttribute("receipt",transaction);
                Double balance = restTemplate.getForObject(BASE_URI_BALANCE, Double.class,customer_acc,customer_pin);
                modelMap.addAttribute("balance",balance);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "receipt";
        }

    }
}
